# CICD Operators

You can use the root of this Application to install both the Tekton (OpenShift
Pipelines) and knative (OpenShift Serverless) operators on your cluster. If you
only want one of them use the respective sub-directory as your base.

# Kustomization

## Operator Versions

Use a patchesStrategicMerge to override the operator channel, for example:

```
patchesStrategicMerge:
- |
  apiVersion: operators.coreos.com/v1alpha1
  kind: Subscription
  metadata:
    name: openshift-pipelines-operator-rh
    namespace: openshift-operators
  spec:
    channel: <different version>
```

## KnativeServing HA

The generated `KnativeServing` resource can take some spec regarding custom
certificates and HA (replicas). [Check the documentation for your version of
OpenShift](https://docs.openshift.com/container-platform/4.9/serverless/install/installing-knative-serving.html)
for the supported configurations.

These can be added with a `patchesStrategicMerge` as well:
```
patchesStrategicMerge:
- |
  apiVersion: operator.knative.dev/v1alpha1
  kind: KnativeServing
  metadata:
    name: knative-serving
    namespace: knative-serving
  spec:
    controller-custom-certs:
      name: something
      type: Secret
    high-availability:
      replicas: 2
```
