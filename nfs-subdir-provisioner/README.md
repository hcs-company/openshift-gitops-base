# NFS Subdir Provisioner

This Application deploys the NFS Subdir Provisioner from here [https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner).

# Warning

NFS subdir provisioning is **not** secure. Use this only when testing and other
forms of shared storage are not available.Also, not all OpenShift components,
like logging and monitoring, like having their storage on NFS. Use local
volumes or a form of block storage for those.

# Kustomization

Out of the box this Application will not work,you will need to override some
variables for the Deployment to point to an existing, accessible, NFS export.
You can do this with a `patchesStrategicMerge`, for example:

```yaml
patchesStrategicMerge:
- |
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: nfs-subdir-provisioner
    namespace: openshift-nfs-subdir-provisioner
  spec:
    template:
      spec:
        containers:
          - name: nfs-client-provisioner
            env:
              - name: NFS_SERVER
                value: <NFS-SERVER-IP-OR-HOST>
              - name: NFS_PATH
                value: <NFS-EXPORT-PATH>
        volumes:
          - name: nfs-client-root
            nfs:
              server: <NFS-SERVER-IP-OR-HOST>
              path: <NFS-EXPORT-PATH>
```

