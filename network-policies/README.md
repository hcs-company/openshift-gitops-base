# Default Network Policies

This Application sets the OpenShift default ProjectRequest Template to a
template with the three standard NetworkPolicies enabled:

- allow-same-namespace
- allow-from-openshift-ingress
- allow-from-openshift-monitoring

Overlays can be added to add extra object into the template if desired, using
`patchesStrategicMerge`

An overlay can also be used to add a `spec.projectRequestMessage` to the
project.config.openshift.io configuration.
