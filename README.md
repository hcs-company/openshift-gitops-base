# Openshift Gitops Base

Base configurations for OpenShift, meant to be used with Openshift GitOps and kustomize

## Warning

While you can use **some** of these configurations as-is most will need
kustomization and personalisation to match your intended cluster. Also, not all
components should typically be used at the same time.
